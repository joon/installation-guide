LaTeX/LyX
=========

Global location of ``.bib`` file.
---------------------------------
http://tex.stackexchange.com/questions/49126/common-bib-file-for-several-collaborators

As in GNU/Linux, you can make ``texmf\bibtex\bib`` directory under the user's
profile directory (``C:\Users\username``).

The following is copied from the stackexchange entry:


#. On Windows with TeX Live, make a ``texmf\bibtex\bib`` directory under the
   user's profile directory (``C:\Users\username`` by default on Windows Vista and
   higher).
#. Place the .bib file in that folder.
#. Place any custom bibliography styles in a ``texmf\bibtex\bst`` directory under
   the user's profile directory.
#. Pull up a command prompt, and run ``mktexlsr texmf`` from the profile directory
   (I don't remember if TeX Live automatically has this in the path or not, so
   you may need to run it as ``C:\texlive\2011\bin\win32\mktexlsr texmf`` or
   similar instead).
#. Write your documents wherever, and use the regular \bibliography and
   related commands as usual. No need for paths, since your personal texmf
   tree will automatically be searched for support files.


