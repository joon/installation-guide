Devel Environment
=================

General
-------

.. code-block:: sh

   setx -m PATH "%PATH%;C:\Users\joon\Programs\ctags58"

Git for Windows
---------------

http://msysgit.github.io/

`Download
<http://code.google.com/p/msysgit/downloads/list?q=full+installer+official+git>`_ 

* Install it with default settings (I tend to use option 3 so unix commands
  are available in other shell)
* It is kinda inevitable to use Git Bash, Traditional CMD, and/or PowerShell
  at the same time in Windows.

   * In the Git Bash shell, ``.bat`` files will not run - for example Sphinx's
     ``make html`` will not run
   * In the PowerShell, ``mklink`` will not run
   * CMD is so old and bad

Git Bash command line::

   C:\Windows\SysWOW64\cmd.exe /c ""C:\Program Files (x86)\Git\bin\sh.exe" --login -i"

posh-git
--------

https://github.com/dahlbyk/posh-git

A set of PowerShell scripts which provide Git/PowerShell integration



Terminal
--------

Console2

* https://github.com/tstone/console2-monokai
* https://github.com/stevenharman/console2-solarized
* https://github.com/alanstevens/console2-ir_black

Color settings::

		<colors>
			<color id="0" r="39" g="40" b="34"/>
			<color id="1" r="229" g="34" b="34"/>
			<color id="2" r="166" g="227" b="45"/>
			<color id="3" r="252" g="149" b="30"/>
			<color id="4" r="118" g="76" b="255"/>
			<color id="5" r="250" g="37" b="115"/>
			<color id="6" r="103" g="217" b="240"/>
			<color id="7" r="225" g="225" b="218"/>
			<color id="8" r="104" g="104" b="104"/>
			<color id="9" r="229" g="24" b="24"/>
			<color id="10" r="177" g="227" b="50"/>
			<color id="11" r="252" g="82" b="9"/>
			<color id="12" r="72" g="102" b="255"/>
			<color id="13" r="250" g="69" b="148"/>
			<color id="14" r="28" g="219" b="240"/>
			<color id="15" r="255" g="255" b="255"/>
		</colors> 

			<color id="9" r="3" g="131" b="245"/>

ConEmu



GSL
---

Download
~~~~~~~~

You can download binary GSL from `oscats 
<https://code.google.com/p/oscats/downloads/list>`_ projects. Make sure you
get the correct architecture.


Environmental Variables
~~~~~~~~~~~~~~~~~~~~~~~

Add the path to \bin directory of your GSL installation to your PATH
Environmental variable::

   setx -m PATH "%PATH%;C:\Users\joon\lib\GSL\bin"

Add an environmental variable LIB_GSL with the path to your GSL installation::

   setx -m LIB_GSL "C:/Users/joon/lib/GSL"

.. note:: You need to run above commands in administrator mode in CMD.


MinGW with OpenMP support
-------------------------

MinGW doesn’t come with openmp by default so if it’s not included in the
Anaconda version you’ll probably have to find the correct installer from the
MinGW site and install it.

https://groups.google.com/a/continuum.io/forum/#!topic/anaconda/c-4u6C29hYM

Install TDM-GCC
~~~~~~~~~~~~~~~

`TDM-GCC <http://tdm-gcc.tdragon.net/>`_ is a compiler suite for Windows.

    It combines the most recent stable release of the GCC toolset with the
    free and open-source MinGW or MinGW-w64 runtime APIs to create a LIBRE
    alternative to Microsoft's compiler and platform SDK

In installation, you can check the openmp support:

.. figure:: images/TDM-GCC-setup.PNG
   :scale: 100 %
   :alt: TDM-GCC Installation

   TDM-GCC Installation

