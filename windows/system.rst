System Settings
===============

Preview Handlers
----------------

PDF Preview
~~~~~~~~~~~

For PDF Preview in Explorer and Directory Opus, get `PDF XChange Viewer
<http://www.tracker-software.com/product/pdf-xchange-viewer/download>`_ and
install Shell Extensions. It is by default checked when you install it.
Portable version does not have this plugin.

See http://www.tracker-software.com/shell_ext.html for explanations.

Source Preview Handler
~~~~~~~~~~~~~~~~~~~~~~

http://www.smartftp.com/client/addons/sourcepreview


PowerShell
----------

If scripts are not enabled, run PowerShell as Administrator and call::

    Set-ExecutionPolicy RemoteSigned -Scope CurrentUser -Confirm

Emacs
-----

Pinning Emacs to the taskbar
~~~~~~~~~~~~~~~~~~~~~~~~~~~~

http://superuser.com/questions/259146/why-latest-emacs-version-dont-support-windows-7-taskbar

If you just pin Emacs icon on the taskbar, next time you click it to launch
Emacs, it will have a terminal window as well as GUI window. In order to fix
this,

1. Run ``runemacs.exe`` with no pre-existing icon on the taskbar.
2. Right click on the running Emacs icon in the taskbar, and click on "pin
   this program to taskbar" item.
3. Close the Emacs
4. Shift right-click on the pinned Emacs icon on the taskbar, click on
   Properties, and change the target from ``emacs.exe`` to ``runemacs.exe``.


공인인증서
~~~~~~~~~~

The location of 공인인증서 in Windows is::

   %APPDATA%\..\LocalLow\NPKI

