Anaconda Installation
=====================

Download
--------

Anaconda download page:
https://store.continuum.io/cshop/anaconda

Path setup
----------

.. code-block:: sh

   export anaconda=C:\Anaconda

Installation of Mayavi
----------------------

http://www.lfd.uci.edu/~gohlke/pythonlibs/ maintains fantastic repository of
binary Python libraries. To install Mayavi, you need to install the following
two packages::

   VTK-5.10.1.win-amd64-py2.7
   ets-4.3.0.win-amd64-py2.7

For some reason, if you install ``VTK-Qt-5.10.1.win-amd64-py2.7`` yields DLL
error.

Installation of ViTables
------------------------

From http://www.lfd.uci.edu/~gohlke/pythonlibs/, download the following
packages::

   PyQt-Py2.7-x64-gpl-4.9.6-1.‌exe
   ViTables-2.1.win-amd64-py2.7.‌exe

You can run ViTables with the following commands (use it as shortcut target)::

   C:\Anaconda\pythonw.exe "C:\Anaconda\Scripts\vitables"

Update Packages
---------------

Using conda
~~~~~~~~~~~

.. note::
   
   Make sure you run ``conda`` with all python app closed. Otherwise, it might
   fail with permission error.

.. code-block:: sh

   $ conda update matplotlib pandas scipy


Using pip
~~~~~~~~~

.. code-block:: sh

   $ pip install bottleneck


Manual updates
~~~~~~~~~~~~~~

IPython
+++++++

.. code-block:: sh

   $ rm -rf $anaconda/Lib/site-packages/IPython $anaconda/Lib/site-packages/ipython*
   $ python setup.py install

Cython
++++++

.. code-block:: sh

   $ rm -rf $anaconda/Lib/site-packages/Cython* $anaconda/Lib/site-packages/cython.*
   $ python setup.py install


CythonGSL
+++++++++

.. code-block:: sh

   $ $EPD/bin/python setup.py install


Statsmodels
+++++++++++

.. code-block:: sh

   $ rm -rf $anaconda/Lib/site-packages/statsmodels*
   $ pip install patsy

   $ git clone git@github.com:joonro/statsmodels.git
   $ cd Statsmodels
   $ python setup.py install

