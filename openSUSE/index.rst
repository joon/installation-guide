.. Installation Guide documentation master file, created by
   sphinx-quickstart on Mon Apr  1 11:39:46 2013.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

openSUSE
========

Contents:

.. toctree::
   :maxdepth: 2

   system
   packages
   EPD
   pyqt
   virtualenv
   backup


