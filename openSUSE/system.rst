System Settings
===============

Root home Settings
------------------

.. code-block:: sh

   $ su
   $ chsh
   $ cd ~
   $ for f in .vim .vimrc .zshrc .oh-my-zsh; do; ln -s /home/joon/$f .; done;

Font Settings
-------------

Generic Fonts Settings (Monospace, Serif, etc)

openSUSE applies fonts in order following files defined in
/etc/fonts/conf.d/56-user.conf and then /etc/fonts/suse-post-user.conf. By
default, the Microsoft fonts are on the top of the lists. Hence, when you
install Microsoft fonts, they will become generic fonts and they look
ugly. Hence make sure you move those fonts (Times New Roman, Arial, Consolas,
etc) lower than Linux fonts such as Dejavu-Sans.

Put your settings into ``~/.config/fontconfig/fonts.conf`` and then add the
following to ``/etc/fonts/conf.d/56-user.conf``::

   <include ignore_missing="yes">~/.config/fontconfig/fonts.conf</include>

ssh Server Setting
------------------

.. code-block:: sh

   $ su

Change the ssh server settings::

   $ vim /etc/ssh/sshd_config

check the ssh server status::

   $ sudo netstat --inet -lpn | grep sshd

restart ssh server::

   $ sudo /etc/init.d/sshd restart

In YaST, open a firewall port::
 
   Allowed Services
   Advanced...
   Add your port to TCP port

You have to install ``terminfo`` to fix  ``Error opening terminal: cygwin.``
error:: 

   zypper in terminfo

Also, if you get ``Error: Can't open display:`` error when you login, you need
to manually export DISPLAY::

   export DISPLAY=:0.0

Disable Touchpad while typing
-----------------------------

http://forums.opensuse.org/archives/sls-archives/archives-suse-linux/archives-general-questions/383041-syndaemon.html

Just add the following line to ``/etc/rc.d/boot.local``:

.. code-block:: sh

   syndaemon -i 1 -d -t -K

If you want to disable the touchpad completely::

   synclient TouchpadOff=1

