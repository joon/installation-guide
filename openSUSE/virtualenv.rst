virtualenv and virtualenvwrapper
================================

Install pip
-----------

.. code-block:: sh

    $ sudo zypper install python-pip

or

.. code-block:: sh

    $ sudo easy_install pip

Installation of virtualenv and virtualenvwrapper on GNU/Linux:
--------------------------------------------------------------

.. code-block:: sh

    $ sudo zypper install python-virtualenvwrapper

or

.. code-block:: sh

    $ sudo pip install virtualenvwrapper

or

.. code-block:: sh

    $ sudo easy_install python-virtualenvwrapper


Wrapping Enthought Python Distribution
--------------------------------------

Note that since you will have multiple projects using EPD, it is not good to
make this EPD virtualenv and install stuff which will be used in many
different project on it. Install common packages to EPD directory, and make
virtualenvs for each of your projects.


Make virtualenvs: epd
~~~~~~~~~~~~~~~~~~~~~

.. code-block:: sh

    $ mkvirtualenv epd --python=$EPD/bin/python --system-site-packages

Make virtualenvs: downloadable-contents-game
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

.. code-block:: sh

  
    $ mkvirtualenv dlc --python=$EPD/bin/python --system-site-packages
    $ workon dlc && cd ~/Dropbox/downloadable-contents-game/simulation && setvirtualenvproject $VIRTUAL_ENV $(pwd)

Make virtualenvs: elimination-used-market-game
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

.. code-block:: sh

    $ mkvirtualenv elimination-used-market-game --python=$EPD/bin/python --system-site-packages
    $ workon elimination-used-market-game && cd ~/Dropbox/elimination-used-market-game/paper && setvirtualenvproject $VIRTUAL_ENV $(pwd)

Make virtualenvs: variety-seeking-movie
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

.. code-block:: sh

    $ mkvirtualenv variety-seeking-movie --python=$EPD/bin/python --system-site-packages
    $ workon variety-seeking-movie && cd ~/Dropbox/variety-seeking-movie/datawork && setvirtualenvproject $VIRTUAL_ENV $(pwd)


Make virtualenvs: sphinx
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

.. code-block:: sh

    $ mkvirtualenv sphinx --system-site-packages
    $ workon sphinx && cd ~/Dropbox/docs && setvirtualenvproject $VIRTUAL_ENV $(pwd)

Add EPD's bin to the PATH of virtuanenv:
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

.. code-block:: sh

    $ vim $WORKON_HOME/dlc/bin/activate

Change the following:

.. code-block:: sh

    _OLD_VIRTUAL_PATH="$PATH"
    PATH="$VIRTUAL_ENV/bin:$PATH"
    export PATH

To:

.. code-block:: sh

    _OLD_VIRTUAL_PATH="$PATH"
    PATH="$VIRTUAL_ENV/bin:$EPD/bin:$PATH"
    export PATH

Show virtualenv's name on the name of Konsole tab automatically
---------------------------------------------------------------

Add the following to the end of ``$WORKON_HOME/postactivate``::

   qdbus $KONSOLE_DBUS_SERVICE $KONSOLE_DBUS_SESSION setTitle 1 `basename $VIRTUAL_ENV`

Back to the default (put the following line to the ``$WORKON_HOME/postdeactivate``::

   qdbus $KONSOLE_DBUS_SERVICE $KONSOLE_DBUS_SESSION setTitle 1 "Konsole: %d : %n"

