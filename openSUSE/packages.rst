Packages Installation
=====================

Add Repos
---------

.. code-block:: sh

   $ baseURL=http://download.opensuse.org/repositories
   $ verSUSE=12.3 && SUSE=openSUSE_"$verSUSE" && verKDE=410


Shell
-----

.. code-block:: sh

   $ zypper in zsh screen
   $ chsh
   $ /bin/zsh


Editors
-------

.. code-block:: sh

   $ zypper in emacs gvim kate


Devel
-----

.. code-block:: sh

   $ zypper ar -f -n devel:tools:scm "$baseURL"/devel:/tools:/scm/"$SUSE"/ devel:tools:scm
   $ zypper in libpng12-0 gsl-devel
   $ zypper in gcc gcc-c++ gcc-fortran git git-cola qgit 


KDE
---

.. code-block:: sh

   $ zypper in kalarm kdegraphics-thumbnailers krename krusader ksshaskpass 
   $ zypper in plasma-theme-caledonia plasma-theme-glassified plasma-theme-nitrogenium
   $ zypper in kde4-ROSA-icontheme

Python
------

.. code-block:: sh

   $ zypper ar -f -n devel:languages:python "$baseURL"/devel:/languages:/python/"$SUSE"/ devel:languages:python
   $ zypper in python-mechanize python-pycallgraph python-rst2pdf python-sphinx python-virtualenvwrapper RunSnakeRun

For ``spynner``::

   $ zypper in python-devel libxml2-devel libxslt-devel

Utilities
---------

.. code-block:: sh

   $ zypper in autokey-common autokey-qt backintime-kde libnotify-tools unison qsynergy
   $ zypper ar -f -n google-talkplugin http://dl.google.com/linux/talkplugin/rpm/stable/x86_64 google-talkplugin
   $ zypper in google-talkplugin

Multimedia
----------

.. code-block:: sh

   $ zypper in comix transmission-qt umplayer youtube-dl


Fonts & Hangul
--------------

.. code-block:: sh

   $ zypper ar -f -n M17N:fonts "$baseURL"/M17N:/fonts/"$SUSE"/ M17N:fonts
   $ zypper in fetchmsttfonts
   $ zypper in make gcc gtk2-devel gtk2-immodule-xim gtk3-devel gtk3-immodule-xim libhangul-devel 
   $ zypper in cantarell-fonts google-inconsolata-fonts kde-oxygen-fonts
   $ zypper in nanum-fonts nanum-gothic-coding-fonts ubuntu-fonts

LaTeX
-----

.. code-block:: sh

   $ zypper in kile lyx


